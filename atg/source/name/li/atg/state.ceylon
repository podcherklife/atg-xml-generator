import ceylon.collection {
	LinkedList,
	HashSet,
	HashMap
}
import name.li.atg.commands {
	Command,
	SetProperty,
	RemoveItem,
	AddItem,
	UpdateItem,
	Id
}

shared class State {
	HashMap<Property,String?> internalState;

	shared new (Map<Property,String?> state) {
		this.internalState = HashMap { entries = state.sequence(); };
	}

	shared new empty() extends State(HashMap<Property,String?>()) {
	}

	shared State modify(Property prop, String? val) {
		value stateCopy = internalState.clone();
		stateCopy.put(prop, val);
		return State(stateCopy);
	}

	shared String? get(Property prop) {
		return internalState.get(prop);
	}

	shared actual Boolean equals(Object that) {
		if (is State that) {
			return internalState == that.internalState;
		} else {
			return false;
		}
	}

	shared Map<Property, String?> map => internalState;

	shared actual String string => "State: ``internalState``";
}

shared Command[] generateCommands(State initialState, State targetState) {

	Command[] commands = [];
	value snapshot = "generatedTransition";
	if (targetState == State.empty() && initialState == State.empty()) {
		return commands; // nothing to do since item did not exist and should not exist
	} else if (targetState == State.empty() && initialState != State.empty()) {
		assert (exists id = initialState.get(itemId));
		assert (exists itemDescriptor = initialState.get(itemDescriptor));
		return commands.withTrailing(RemoveItem(Id(id, itemDescriptor), snapshot));
	} else if (targetState != State.empty() && initialState == State.empty()) {
		assert (exists id = targetState.get(itemId));
		assert (exists itemDescriptor = targetState.get(itemDescriptor));
		return commands.withTrailing(AddItem(Id(id, itemDescriptor), snapshot, generateMutationCommands(initialState, targetState)));
	} else {
		checkIdIsTheSame(initialState, targetState);
		checkItemDescriptorIsTheSame(initialState, targetState);
		assert (exists id = targetState.get(itemId));
		assert (exists itemDescriptor = targetState.get(itemDescriptor));
		assert(nonempty mutations = generateMutationCommands(initialState, targetState));
		return commands.withTrailing(UpdateItem(Id(id, itemDescriptor), snapshot, mutations));
	}
}


void compareProperties(Property prop, State state1, State state2) {
	value val1 = state1.map.get(prop);
	value val2 = state2.map.get(prop);

	"Property prop is not the same: ``val1 else "null"`` != ``val2 else "null"``"
	assert (nullalbeEquals(val1, val2));
}

Anything(State, State) checkItemDescriptorIsTheSame = curry(compareProperties)(itemDescriptor);
Anything(State, State) checkIdIsTheSame = curry(compareProperties)(itemId);

SetProperty[] generateMutationCommands(State initialState, State targetState) {
	value mutations = LinkedList<SetProperty>();
	value allKnownProperties = HashSet { elements = initialState.map.keys.chain(targetState.map.keys); };
	for (property in allKnownProperties) {
		if (property in [itemDescriptor, itemId]) {
			continue;
		}

		value initialPropertyValue = initialState.get(property);
		value targetPropertyValue = targetState.get(property);

		if (nullalbeEquals(initialPropertyValue, targetPropertyValue)) {
			// do nothing
		} else {
			mutations.add(SetProperty(property, targetPropertyValue));
		}
	}
	return mutations.sequence();
}