import name.li.atg.commands {
	Id
}
shared abstract class Event() of MutationEvent | CreationEvent | DeletionEvent | SnapshotEvent {
}

shared class MutationEvent(shared Property property, shared String? val) extends Event() {
	shared actual Boolean equals(Object that) {
		if (is MutationEvent that) {
			return property==that.property && nullalbeEquals(val, that.val);
		} else {
			return false;
		}
	}

	shared actual Integer hash => property.hash;

	shared actual String string => "Mutation ``property`` -> `` val else "null" ``";
}

shared class CreationEvent(shared Id id) extends Event() {
	shared actual Boolean equals(Object that) {
		if (is CreationEvent that) {
			return id == that.id;
		} else {
			return false;
		}
	}

	shared actual Integer hash => id.hash;

	shared actual String string => "Creation ``id``";
}

shared class DeletionEvent(shared Id id) extends Event() {
	shared actual Boolean equals(Object that) {
		if (is DeletionEvent that) {
			return id == that.id;
		} else {
			return false;
		}
	}

	shared actual Integer hash => id.hash;

	shared actual String string => "Deletion ``id``";
}

shared class SnapshotEvent(shared String snapshot) extends Event() {
	shared actual Boolean equals(Object that) {
		if (is SnapshotEvent that) {
			return snapshot == that.snapshot;
		} else {
			return false;
		}
	}
	shared actual Integer hash => snapshot.hash;

	shared actual String string => "Snapshot ``snapshot``";
}
