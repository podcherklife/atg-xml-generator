import ceylon.collection {
	HashMap,
	LinkedList
}

import name.li.atg.commands {
	Command
}

shared interface CommandsConsumer  {
	shared formal void consumeCommand(Command command);
}

shared interface TrackingStrategy {
	shared formal String? resolveTargetId(Command command, Map<String, RepositoryItem> items);
}

shared object trackById satisfies TrackingStrategy {
	shared actual String? resolveTargetId(Command command, Map<String, RepositoryItem> items) {
		return command.id.val;
	}
}

shared class TrackByProperty(Property property) satisfies TrackingStrategy {

	shared actual String? resolveTargetId(Command command, Map<String,RepositoryItem> items) {

		// try to find tracking property in command itself (should work for AddItem and sometimes for UpdateItem commands
		// if that fails, try to find existing item with the id from the command
		String? trackingId = getTrackingIdUsingNewItem(command) else getTrackingIdUsingRealId(command, items);

		return trackingId;
	}

	String? getTrackingIdUsingRealId(Command command, Map<String,RepositoryItem> items) {
		value item = items.items.find((item) => item.currentId == command.id);
		return item?.currentState?.get(property);
	}

	String? getTrackingIdUsingNewItem(Command command) {
		// to find property we can create new item, apply commands to it and then get property value
		// instead of traversing graph of Command object
		value newItemToApplyCommandsTo = RepositoryItem();
		newItemToApplyCommandsTo.acceptCommand(command);
		value properties = newItemToApplyCommandsTo.currentState.map;
		return properties.find((pair) => pair.key == property)?.item;
	}

}

shared class ItemRegistry(TrackingStrategy trackBy = trackById) satisfies CommandsConsumer {

	value items = HashMap<String, RepositoryItem>();
	value brokenCommands = LinkedList<Command>();

	shared actual void consumeCommand(Command command) {
		value trackingId = trackBy.resolveTargetId(command, items);
		if (exists trackingId) {
			if (exists item = items[trackingId]) {
				item.acceptCommand(command);
			} else {
				value newItem = RepositoryItem();
				newItem.acceptCommand(command);
				items[trackingId] = newItem;
			}
		} else {
			brokenCommands.add(command);
		}
	}

	shared RepositoryItem[] targetItems(Command[] commands) {
		value ids = commands.map((command) => trackBy.resolveTargetId(command, items));
		return items.filter((key->val) => key in ids)*.item;
	}

	shared RepositoryItem? itemWithId(String itemId) => items.get(itemId);

	shared RepositoryItem[] allItems => items.sequence().map((pair) => pair.item).sequence();

	shared Command[] allBrokenCommands => brokenCommands.sequence();

}