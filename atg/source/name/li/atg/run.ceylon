import ceylon.file {
	File,
	Path,
	parsePath,
	Directory
}

import java.io {
	Writer,
	JFile=File,
	FileWriter,
	PrintWriter
}
import java.lang {
	System
}
import java.util.regex {
	Pattern
}

import name.li.atg.commands {
	CsvParser,
	PrefixBasedSequentialIdGenerator,
	parseXml,
	XmlGenerator,
	Command
}


Pattern timestampPattern = Pattern.compile("(\\d+)");

Comparison byTimeStamp(File f1, File f2) {
	String getTimeStamp(String name) {
		value matcher = timestampPattern.matcher(name);
		"Timestamp not found in filename ``name```"
		assert (true == matcher.find());
		return matcher.group(0);
	}
	return getTimeStamp(f1.name).compare(getTimeStamp(f2.name));
}

void parseBaseline(Path path, ItemRegistry registry) {
	value dir = path.resource;
	"Baseline should be directory"
	assert(is Directory dir);

	try {
		for (file in dir.files().sort(byTimeStamp)) {
			parseXml(file.path.absolutePath.string, registry);
		}
	} catch(Exception e) {
		throw Exception("Failed to parse file ``path``", e);
	}
}

void generateXml([Command*] modificationCommands, Writer writer) {
	XmlGenerator().generate(writer, modificationCommands);
	PrintWriter(writer).print('\n');
	writer.flush();
}

Writer fileWriter(String path) {
	return FileWriter(JFile(path));
}

Writer printingWriter = PrintWriter(System.\iout);


Command[] getCommandsFromCsv(Path csvFilePath, String generatedIdPrefix) {
	"Csv file expeced"
	assert(is File file = csvFilePath.resource);
	"Only *.csv files supported"
	assert(file.name.endsWith(".csv"));



	value consumer = CollectingCommandsConsumer();
	CsvParser("message").readCommands(getLines(file), consumer, "newChanges", PrefixBasedSequentialIdGenerator(generatedIdPrefix));
	return consumer.collectedCommands;
}

Iterator<String> getLines(File file) {
	value reader = file.Reader();
	object iterator satisfies Iterator<String> {
		shared actual Finished|String next() {
			if (exists next = reader.readLine()) {
				return next;
			} else {
				reader.close();
				return finished;
			}
		}
	}
	return iterator;
}

"Run the module `atg`."
shared void run() {
	value baselineDir = process.namedArgumentValue("baseline");
	"Required --baseline parameter"
	assert(exists baselineDir);

	value csv = process.namedArgumentValue("input");
	"Required --input parameter"
	assert (exists csv);
	
	value generatedIdPrefix = process.namedArgumentValue("generatedIdPrefix") else "sample";

	value applyChanges = process.namedArgumentValue("applyChanges");
	value applyChangesWriter = if (exists applyChanges) then fileWriter(applyChanges) else printingWriter;

	value rollbackChanges = process.namedArgumentValue("rollbackChanges");
	value rollbackChangesWriter = if (exists rollbackChanges) then fileWriter(rollbackChanges) else printingWriter;

	value trackByArg = process.namedArgumentValue("trackBy");
	value trackBy = if (exists trackByArg) then TrackByProperty(Property(trackByArg)) else trackById;

	value registry = ItemRegistry(trackBy);

	parseBaseline(parsePath(baselineDir), registry);

	value commandsToApply = getCommandsFromCsv(parsePath(csv), generatedIdPrefix);

	commandsToApply.each(registry.consumeCommand);

	value modifiedItems = registry.targetItems(commandsToApply);

	value postStatePairs = modifiedItems.map((item) {
		assert (exists preModifiedState = item.allSnapshots.last);
		return [preModifiedState, item.currentState];
	});


	value modificationCommands = postStatePairs.flatMap((states) => generateCommands(states[0], states[1])).sequence();
	generateXml(modificationCommands, applyChangesWriter);

	value rollbackCommands = postStatePairs.flatMap((states) => generateCommands(states[1], states[0])).sequence();
	generateXml(rollbackCommands, rollbackChangesWriter);

}