import ceylon.collection {
	HashMap
}
import name.li.atg.commands {
	AddItem,
	Command,
	UpdateItem,
	SetProperty,
	RemoveItem,
	Id
}
shared class Property(shared String name) {
	shared actual Boolean equals(Object that) {
		if (is Property that) {
			return name == that.name;
		} else {
			return false;
		}
	}

	shared actual Integer hash => name.hash;

	shared actual String string => "Property ``name``";
}

shared  object itemId extends Property("id") {
}

shared object itemDescriptor extends Property("item-descriptor") {

}

String  initialSnapshotName = "initialState";

shared class RepositoryItem {

	variable String currentSnapshotName = initialSnapshotName;
	variable State state = State.empty();
	HashMap<String, State> snapshots = HashMap<String, State>();
	variable CommandHandlingStrategy eventGenerator;
	variable CommandHandlingStrategy nonExistantGenerator;
	variable [Event*] log = [];

	shared new(CommandHandlingStrategy nonExistantStateCommandHandlingStrategy = NonExistantAutoCreateOnMutation()) {
		this.nonExistantGenerator = nonExistantStateCommandHandlingStrategy;
		this.eventGenerator = nonExistantGenerator;
	}

	shared new strict() extends RepositoryItem(NonExistant()) {
	}

	shared void acceptCommand(Command command) {
		for (event in createEventsFromCommand(command)) {
			acceptEvent(event);
		}
	}

	shared void acceptEvent(Event event) {
		log = log.withTrailing(event);
		switch (event)
		case (is CreationEvent) {
			state = state.modify(itemId, event.id.val);
			state = state.modify(itemDescriptor, event.id.itemDescriptor);
			eventGenerator = Normal();
		}
		case (is MutationEvent) {
			state = state.modify(event.property, event.val);
		}
		case (is DeletionEvent) {
			state = State.empty();
			eventGenerator = NonExistantAutoCreateOnMutation();
		}
		case (is SnapshotEvent) {
			makeSnapshot(event);
		}
	}

	shared void makeSnapshot(SnapshotEvent event) {
		if (snapshots.defines(event.snapshot)
			&& currentSnapshotName != event.snapshot) { // XXX: some files have the same item updated twice, so we need this hack
			throw Exception("Snapshot with the name ``event.snapshot`` already exists for item ``state.get(itemId) else "null" ``");
		}
		snapshots.put(currentSnapshotName, state);
		currentSnapshotName = event.snapshot;
	}

	shared [Event*] createEventsFromCommand(Command command) {
		return eventGenerator.handleCommand(command);
	}

	shared State currentState => this.state;

	shared State? snapshot(String snapshotName) {
		return snapshots.get(snapshotName);
	}

	shared State initialState {
		if (currentSnapshotName == initialSnapshotName) {
			return state;
		} else {
			assert(exists initialState = snapshot(initialSnapshotName));
			return initialState;
		}
	}

	shared Event[] allEvents => log;

	shared State[] allSnapshots => snapshots.items.sequence();

	//XXX: this code assumes that id does not change during the item's life
	shared Id currentId => Id(state.get(itemId) else "", state.get(itemDescriptor) else "");
}

shared abstract class CommandHandlingStrategy() of Normal | NonExistant {
	shared formal [Event*] handleCommand(Command|SetProperty command);
}

class Normal() extends CommandHandlingStrategy() {
	shared actual [Event*] handleCommand(Command|SetProperty command) {
		switch (command)
		case (is AddItem) {
			return [
				SnapshotEvent(command.snapshot),
				*command.mutations.map(handleCommand).flatMap(identity).sequence()
			];
		}
		case (is UpdateItem) {
			return [
				SnapshotEvent(command.snapshot),
				*command.mutations.map(handleCommand).flatMap(identity).sequence()
			];
		}
		case (is SetProperty) {
			return [
				MutationEvent(command.prop, command.val)
			];
		}
		case (is RemoveItem) {
			return [
				DeletionEvent(command.id)
			];
		}
	}
}

class NonExistant() extends CommandHandlingStrategy() {
	shared actual default [Event*] handleCommand(Command|SetProperty command) {
		switch (command)
		case (is AddItem) {
			return [
				SnapshotEvent(command.snapshot),
				CreationEvent(command.id),
				*command.mutations.map((m) => MutationEvent(m.prop, m.val))
			];
		}
		else {
			throw Exception("Can handle only `` `class AddItem` `` command since item does not exist at this point, but passed command is ``command``");
		}
	}
}


// XXX: files are inconsistent and may update or remove item that does not exist
class NonExistantAutoCreateOnMutation() extends NonExistant() {

	shared actual [Event*] handleCommand(Command|SetProperty command) {
		switch (command)
		case (is AddItem) {
			return super.handleCommand(command);
		}
		case (is UpdateItem) {
			return [
				CreationEvent(command.id),
				*Normal().handleCommand(command)
			];
		}
		case (is RemoveItem) {
			return []; // do nothing
		}
		else {
			throw Exception("Cannot handle ``command``");
		}
	}

}