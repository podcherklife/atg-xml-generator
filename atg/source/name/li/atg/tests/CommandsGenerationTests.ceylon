import ceylon.collection {
	HashSet
}
import ceylon.test {
	test,
	assertEquals,
	assertThatException
}

import name.li.atg {
	Property,
	itemId,
	State,
	generateCommands,
	itemDescriptor
}
import name.li.atg.commands {
	SetProperty,
	AddItem,
	RemoveItem,
	Command,
	UpdateItem,
	Id
}

class EventsGenerationTests() {

	value prop1 = Property("prop1");
	value prop2 = Property("prop2");
	value prop3 = Property("prop3");

	test
	shared void shouldGenerateEventsForIncrementalUpdates() {
		value state1 = State.empty()
			.modify(itemId, "id")
			.modify(itemDescriptor, "idesc")
			.modify(prop1, "foo")
			.modify(prop3, "val3");

		value state2 = State.empty()
			.modify(itemId, "id")
			.modify(itemDescriptor, "idesc")
			.modify(prop2, "bar")
			.modify(prop3, "val31");

		value expectedCommands = [
			UpdateItem(Id("id", "idesc"), "generatedTransition", [
					SetProperty(prop1, null),
					SetProperty(prop2, "bar"),
					SetProperty(prop3, "val31")
				])
			];

		checkCommandsAreGeneratedAsExpected(state1, state2, expectedCommands);
	}

	test
	shared void shouldGenerateEventsForDeletion() {
		value state1 = State.empty()
			.modify(itemId, "foo")
			.modify(itemDescriptor, "idesc")
			.modify(prop1, "prop1");

		value state2 = State.empty();
		value expectedCommands = [RemoveItem(Id("foo", "idesc"), "generatedTransition")];

		checkCommandsAreGeneratedAsExpected(state1, state2, expectedCommands);
	}

	test
	shared void shouldGenerateEventsForCreation() {
		value state1 = State.empty();

		value state2 = State.empty()
			.modify(itemId, "id")
			.modify(itemDescriptor, "idesc")
			.modify(prop1, "prop1");
		value expectedCommands = [AddItem(Id("id", "idesc"), "generatedTransition", [SetProperty(prop1, "prop1")])];

		checkCommandsAreGeneratedAsExpected(state1, state2, expectedCommands);
	}

	test
	shared void shouldFailToGenerateEventsWhenIdIsChanged() {
		value state1 = State.empty()
			.modify(itemDescriptor, "idesc")
			.modify(itemId, "id0");
		value state2 = State.empty()
				.modify(itemId, "id1")
				.modify(itemDescriptor, "idesc")
				.modify(prop1, "prop1");
		assertThatException(() => checkCommandsAreGeneratedAsExpected(state1, state2, [])).hasType(`AssertionError`);
	}

	test
	shared void shouldGenerateRemoveItemCommand() {
		value state1 = State.empty()
			.modify(itemDescriptor, "idesc")
			.modify(itemId, "id")
			.modify(prop1, "val1");

		value state2 = State.empty();
		value expectedCommands = [RemoveItem(Id("id", "idesc"), "generatedTransition")];

		checkCommandsAreGeneratedAsExpected(state1, state2, expectedCommands);
	}


	shared void checkCommandsAreGeneratedAsExpected(State initialState, State targetState, Command[] expectedCommands) {
		value generatedCommands = HashSet { elements = generateCommands(initialState, targetState); };
		value expectedCommandsSet = HashSet { elements = expectedCommands; };
		assertEquals(generatedCommands, expectedCommandsSet);
	}
}
