import java.io {
	StringReader
}
import name.li.atg {
	CommandsConsumer,
	Property
}
import ceylon.collection {
	LinkedList
}
import ceylon.test {
	assertEquals,
	test
}
import name.li.atg.commands {
	AddItem,
	XmlParser,
	Command,
	SetProperty,
	RemoveItem,
	Id
}

class XmlParserTests() {

	test
	shared void shouldParseCommands() {
		String text = """<?xml version="1.0" encoding="UTF-8" standalone="no"?>

		                                     <!--
		                                     This is a simple xml template demonstrating add-item, remove-item, and query-items
		                                     tags.
		                                     -->
		                                     <gsa-template>

		                                     <header>
		                                     <name>Test 1</name>
		                                     <author>Marty</author>
		                                     <version>1</version>
		                                     </header>

		                                     <!-- This defines the item-descriptor -->
		                                     <item-descriptor name="users" default="true">
		                                     <table name="users" id-column-names="id" type="primary">
		                                     <property name="prop1"/>
		                                     </table>
		                                     </item-descriptor>

		                                     <!-- This adds to the database an instance of 'users' with id = 1 -->
		                                     <add-item item-descriptor="users" id="1">
		                                     <set-property name="prop1" value="Marty"/>

		                                     <set-property name="elementText">
		                                     	Sally
		                                     </set-property>

		                                     <set-property name="cdata">
		                                     	<![CDATA[Robbie]]>
		                                     </set-property>

		                                     </add-item>


		                                     <add-item item-descriptor="users" id="13">
		                                     <set-property name="prop1" value="Marty2"/>
		                                     </add-item>

		                                     <add-item item-descriptor="users">
		                                     <set-property name="noIdUserProp" value="NoIdUserVal"/>
		                                     </add-item>

		                                     <remove-item id="removedItemId" item-descriptor="users" />

		                                     <!-- This queries the database for any of 'users' with prop1 = "Marty" -->
		                                     <query-items item-descriptor="users">
		                                     prop1="Marty"
		                                     </query-items>


		                                     </gsa-template>


		                                     """;

		value reader = StringReader(text);

		object storingCommandsConsumer satisfies CommandsConsumer {
			shared LinkedList<Command> storedCommands = LinkedList<Command>();

			shared actual void consumeCommand(Command command) {
				storedCommands.add(command);
			}
		}

		XmlParser("snapshot", reader, storingCommandsConsumer).generateCommands();

		value generatedCommands = storingCommandsConsumer.storedCommands.sequence();

		assertEquals(generatedCommands, [
				AddItem(Id("1", "users"), "snapshot", [
						SetProperty(Property("prop1"), "Marty"),
						SetProperty(Property("elementText"), "Sally"),
						SetProperty(Property("cdata"), "Robbie")
					]),
				AddItem(Id("13", "users"), "snapshot", [
						SetProperty(Property("prop1"), "Marty2")
					]),
				AddItem(Id("snapshot0", "users"), "snapshot", [
					SetProperty(Property("noIdUserProp"), "NoIdUserVal")
				]),
				RemoveItem(Id("removedItemId", "users"), "snapshot")
			]);
	}
}
