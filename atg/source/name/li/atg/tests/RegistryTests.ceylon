import ceylon.collection {
	HashSet
}
import ceylon.test {
	test,
	assertEquals,
	assertThatException
}

import name.li.atg {
	ItemRegistry,
	Property,
	CreationEvent,
	MutationEvent,
	SnapshotEvent,
	TrackByProperty
}
import name.li.atg.commands {
	AddItem,
	SetProperty,
	UpdateItem,
	RemoveItem
}

class RegistryTests() {
	value prop1 = Property("prop1");
	value prop2 = Property("prop2");



	test
	shared void shouldStoreCommandsForWhichResolvingTargetItemFailed() {
		value trackingProperty = prop1;
		value registry = ItemRegistry(TrackByProperty(trackingProperty));

		value commands = [
			AddItem(simpleId("1"), "v1", [
				SetProperty(prop2, "val1")
			]),
			RemoveItem(simpleId("3"), "v2"),
			UpdateItem(simpleId("3"), "v3", [
				SetProperty(prop2, "val3")
			])
		];
		commands.each(registry.consumeCommand);

		assertEquals (
			HashSet { elements = registry.allBrokenCommands; },
			HashSet { elements = commands; }
		);
	}

	test
	shared void shouldBeAbleToUseProeprtyForTracking() {
		value trackingProperty = prop1;
		value registry = ItemRegistry(TrackByProperty(trackingProperty));

		value commands = [
			AddItem(simpleId("1"), "v1", [
				SetProperty(trackingProperty, "propId1"),
				SetProperty(prop2, "val1")
				]),
			UpdateItem(simpleId("3"), "v2", [
				SetProperty(trackingProperty, "propId1"),
				SetProperty(prop2, "val3")
				])
		];

		commands.each(registry.consumeCommand);


		value item = registry.itemWithId("propId1");
		assert(exists item);
		assertEquals(
			HashSet {
				elements = item.allEvents;
			},
			HashSet {
				elements = [
					SnapshotEvent("v1"),
					CreationEvent(simpleId("1")),
					MutationEvent(trackingProperty, "propId1"),
					MutationEvent(prop2, "val1"),
					SnapshotEvent("v2"),
					MutationEvent(trackingProperty, "propId1"),
					MutationEvent(prop2, "val3")
				];
			}
		);

	}

	test
	shared void shouldRouteCommandsToSeparateItemsAndGenerateExpectedState() {
		value registry = ItemRegistry();
		value commands = [
			AddItem(simpleId("123"), "foo", [
					SetProperty(prop1, "1"),
					SetProperty(prop2, "2")
				]),
			UpdateItem(simpleId("123"), "bar", [
					SetProperty(prop1, "11")
				]),
			AddItem(simpleId("qwe"), "foo", [
					SetProperty(prop1, "1"),
					SetProperty(prop2, "2")
				])
		];

		commands.each(registry.consumeCommand);

		value allItems = registry.allItems;

		assert (allItems.size == 2);

		assert (exists item123 = registry.itemWithId("123"));
		assertEquals(
			HashSet {
				elements = item123.allEvents;
			},
			HashSet {
				elements = [
					SnapshotEvent("foo"),
					CreationEvent(simpleId("123")),
					MutationEvent(prop1, "1"),
					MutationEvent(prop2, "2"),
					SnapshotEvent("bar"),
					MutationEvent(prop1, "11")
				];
			});

		assert (exists itemQwe = registry.itemWithId("qwe"));
		assertEquals(
			HashSet {
				elements = itemQwe.allEvents;
			},
			HashSet {
				elements = [
					SnapshotEvent("foo"),
					CreationEvent(simpleId("qwe")),
					MutationEvent(prop1, "1"),
					MutationEvent(prop2, "2")
				];
			});
	}


	test
	shared void shouldFailWhenThereAreTwoNonContiguousSnapshotsWithTheSameName() {
		assertThatException(() {
			value item = newRepositoryItem();
			item.acceptCommand(AddItem(simpleId("foo"), "snashot1", []));
			item.acceptCommand(UpdateItem(simpleId("foo"), "snashot2", [SetProperty(Property("foo"), "bar")]));
			item.acceptCommand(UpdateItem(simpleId("foo"), "snashot1", [SetProperty(Property("foo"), "baz")]));
		}).hasType(`Exception`);
	}

	test
	shared void shouldNotFailWhenThereAreTwoContiguousSnapshotsWithTheSameName() {
		value item = newRepositoryItem();
		item.acceptCommand(AddItem(simpleId("foo"), "snashot1", []));
		item.acceptCommand(UpdateItem(simpleId("foo"), "snashot1", [SetProperty(Property("foo"), "baz")]));
		item.acceptCommand(UpdateItem(simpleId("foo"), "snashot2", [SetProperty(Property("foo"), "bar")]));
	}
}
