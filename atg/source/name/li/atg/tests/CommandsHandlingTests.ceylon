import ceylon.test {
	assertThatException,
	test,
	assertEquals
}

import name.li.atg {
	Property,
	itemId,
	CreationEvent,
	MutationEvent,
	SnapshotEvent
}
import name.li.atg.commands {
	AddItem,
	UpdateItem,
	SetProperty,
	RemoveItem
}
class EventHandlingTests() {

	test
	shared void shouldFailWhenThereAreTwoNonContiguousSnapshotsWithTheSameName() {
		assertThatException(() {
			value item = newRepositoryItem();
			item.acceptCommand(AddItem(simpleId("foo"), "snashot1", []));
			item.acceptCommand(UpdateItem(simpleId("foo"), "snashot2", [SetProperty(Property("foo"), "bar")]));
			item.acceptCommand(UpdateItem(simpleId("foo"), "snashot1", [SetProperty(Property("foo"), "baz")]));
		}).hasType(`Exception`);
	}

	test
	shared void shouldNotFailWhenThereAreTwoContiguousSnapshotsWithTheSameName() {
		value item = newRepositoryItem();
		item.acceptCommand(AddItem(simpleId("foo"), "snashot1", []));
		item.acceptCommand(UpdateItem(simpleId("foo"), "snashot1", [SetProperty(Property("foo"), "baz")]));
		item.acceptCommand(UpdateItem(simpleId("foo"), "snashot2", [SetProperty(Property("foo"), "bar")]));
	}

	test
	shared void shouldCreateItemOnUpdateIfItemDoesNotExist() {
		value item = newRepositoryItem();
		item.acceptCommand(UpdateItem(simpleId("id"), "snapshot", [SetProperty(itemId, "id")]));
		assertEquals(item.allEvents, [CreationEvent(simpleId("id")), SnapshotEvent("snapshot"), MutationEvent(itemId, "id")]);
	}

	test
	shared void shouldFailToHandleUpdateItemCommandWhenItemDoesNotExistInStrictMode() {
		assertThatException(() {
			value item = newStrictRepositoryItem();
			item.acceptCommand(UpdateItem(simpleId("id"), "snapshot", [SetProperty(itemId, "id")]));
		}).hasType(`Exception`);
	}

	test
	shared void shouldIgnoreRemoveItemCommandWhenItemDoesNotExist() {
		value item = newRepositoryItem();
		item.acceptCommand(RemoveItem(simpleId("id"), "snapshot"));
		assertEquals(item.allEvents, []);
	}

	test
	shared void shouldFailToHandleRemoveItemCommandWhenItemDoesNotExistInStrictMode() {
		assertThatException(() {
			value item = newStrictRepositoryItem();
			item.acceptCommand(RemoveItem(simpleId("id"), "snapshot"));
		}).hasType(`Exception`);
	}

}