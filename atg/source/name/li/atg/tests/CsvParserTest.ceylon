import ceylon.test {
	test,
	assertThatException,
	assertEquals
}

import name.li.atg {
	Property,
	CollectingCommandsConsumer
}
import name.li.atg.commands {
	CsvParser,
	PrefixBasedSequentialIdGenerator,
	AddItem,
	SetProperty
}

class CsvParserTest() {
	
	test
	shared void shouldFailWhenHeaderIsMissing() {
		assertThatException(() => CsvParser("").readCommands({}.iterator(), noopCommandsConsumer, "doesNotMatter", PrefixBasedSequentialIdGenerator("")))
			.hasType(`AssertionError`);
	}
	
	test
	shared void shouldGenerateExpectedCommands() {
		value lines = {
			"prop1,prop2,id",
			"val11,val12,nonGenerated",
			"val21,val22,",
			"\"val 31\",val\"\"32\"\","
		};
		
		value commandsConsumer = CollectingCommandsConsumer();
		value idGenerator = ConstantIdGenerator("id");
		
		CsvParser("").readCommands(lines.iterator(), commandsConsumer, "sn", idGenerator);
		
		value commands = commandsConsumer.collectedCommands;
		
		assertEquals(commands, [
				AddItem(simpleId("nonGenerated"), "sn", [
						SetProperty(Property("prop1"), "val11"),
						SetProperty(Property("prop2"), "val12")
					]),
				AddItem(simpleId("id"), "sn", [
						SetProperty(Property("prop1"), "val21"),
						SetProperty(Property("prop2"), "val22")
					]),
				AddItem(simpleId("id"), "sn", [
						SetProperty(Property("prop1"), "val 31"),
						SetProperty(Property("prop2"), "val\"32\"")
					])
			]);
	}
}
