import ceylon.test {
	assertEquals,
	test
}

import name.li.atg {
	Property,
	State,
	itemId,
	itemDescriptor
}
import name.li.atg.commands {
	AddItem,
	SetProperty,
	UpdateItem,
	Id
}

class StateTests() {

	value prop1 = Property("prop1");
	value prop2 = Property("prop2");

	value commands = [
		AddItem(Id("foo", "idesc"), "snapshot1", [
				SetProperty(prop1, "val1")
			]),
		UpdateItem(Id("foo", "idesc"), "snapshot2", [
				SetProperty(prop1, "val0"),
				SetProperty(prop2, "val2")
			])
	];

	test
	shared void shouldGenerateProperStateFromCommands() {

		value item = newRepositoryItem();

		commands.each(item.acceptCommand);

		value expectedState = State.empty()
			.modify(itemId, "foo")
			.modify(itemDescriptor, "idesc")
			.modify(prop1, "val0")
			.modify(prop2, "val2");

		assertEquals(item.currentState, expectedState);
	}

	test
	shared void shouldGenerateSnapshots() {

		value item = newRepositoryItem();

		commands.each(item.acceptCommand);

		value snapshot1 = item.snapshot("snapshot1");
		value expectedState1 = State.empty()
				.modify(itemId, "foo")
				.modify(itemDescriptor, "idesc")
				.modify(prop1, "val1");

		assertEquals(snapshot1, expectedState1);

	}
}
