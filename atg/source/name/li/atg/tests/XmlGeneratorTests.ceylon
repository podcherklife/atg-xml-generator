import ceylon.test {
	test,
	assertEquals,
	assertThatException
}

import java.io {
	StringWriter
}

import name.li.atg {
	Property
}
import name.li.atg.commands {
	AddItem,
	SetProperty,
	XmlGenerator,
	UpdateItem,
	Command,
	RemoveItem,
	Id
}

class XmlGeneratorTests() {

	test
	shared void shouldGenerateProperXmlForAddItemCommand() {
		value commands = [
			AddItem(Id("id", "idesc"), "snapshot", [
				SetProperty(Property("prop1"), "val1"),
				SetProperty(Property("prop2"), "val2")
			])
		];
		value expectedXml = """<?xml version="1.0" ?>
		                       <gsa-template>
		                       <add-item id="id" item-descriptor="idesc">
		                       <set-property name="prop1"><![CDATA[val1]]></set-property>
		                       <set-property name="prop2"><![CDATA[val2]]></set-property>
		                       </add-item>
		                       </gsa-template>
		                       """;
		shouldGenerateXmlFromCommands(expectedXml, commands);
	}

	test
	shared void shouldGenerateProperXmlForUpdateItemCommand() {
		value commands = [
			UpdateItem(Id("id", "idesc"), "sn", [
				SetProperty(Property("prop1"), "val1")
			])
		];

		value expectedXml = """<?xml version="1.0" ?>
		                       <gsa-template>
		                       <update-item id="id" item-descriptor="idesc">
		                       <set-property name="prop1"><![CDATA[val1]]></set-property>
		                       </update-item>
		                       </gsa-template>
		                       """;
		shouldGenerateXmlFromCommands(expectedXml, commands);
	}

	test
	shared void shouldGenerateProperXmlForRemoveItemCommand() {
		value commands = [
			RemoveItem(Id("id", "idesc"), "snapshot")
		];

		value expectedXml = """<?xml version="1.0" ?>
		                       <gsa-template>
		                       <remove-item id="id" item-descriptor="idesc"></remove-item>
		                       </gsa-template>
		                       """;
		shouldGenerateXmlFromCommands(expectedXml, commands);
	}

	shared void shouldGenerateXmlFromCommands(String expectedXml, Command[] commands) {
		value xmlGen = XmlGenerator();
		value sw = StringWriter();
		xmlGen.generate(sw, commands);


		assertEquals(sw.string, expectedXml.replace("\n", ""));
	}

	test
	shared void shouldFailIfCommandsBelongToDifferentSnapshots() {
		value commands = [
			AddItem(simpleId("id"), "snapshot", [
				SetProperty(Property("prop1"), "val1"),
				SetProperty(Property("prop2"), "val2")
			]),
			UpdateItem(simpleId("id"), "sn", [
					SetProperty(Property("prop1"), "val1")
			])
		];

		assertThatException(() {
			value xmlGen = XmlGenerator();
			value sw = StringWriter();
			xmlGen.generate(sw, commands);
		}).hasType(`AssertionError`);
	}

}
