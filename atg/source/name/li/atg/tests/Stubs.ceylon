import name.li.atg {
	CommandsConsumer,
	RepositoryItem
}
import name.li.atg.commands {
	Command,
	IdGenerator,
	Id
}

shared class CallbackCommandsConsumer(Anything(Command) callback) satisfies CommandsConsumer {
	shared actual void consumeCommand(Command command) {
		callback(command);
	}
}

shared object noopCommandsConsumer extends CallbackCommandsConsumer(noop) {
}

shared class ConstantIdGenerator(String val) satisfies IdGenerator {
	shared actual String next() => val;
}

RepositoryItem newRepositoryItem() {
	return RepositoryItem();
}

RepositoryItem newStrictRepositoryItem() {
	return RepositoryItem.strict();
}

Id simpleId(String? val) {
	return Id(val else "generatedId", "");
}