import ceylon.test {
	test,
	assertEquals
}
import name.li.atg.commands {
	PrefixBasedSequentialIdGenerator
}
class IdGeneratorTest() {

	test
	shared void prefixBasedSequentialIdGeneratorShouldReturnSequentialIdsStartingFromZero() {
		value idGenerator = PrefixBasedSequentialIdGenerator("foo");

		assertEquals(
			[idGenerator.next(), idGenerator.next(), idGenerator.next()],
			["foo0", "foo1", "foo2"]);
	}

}