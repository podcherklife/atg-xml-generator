import name.li.atg.commands {
	Command
}
shared Boolean nullalbeEquals(Object? left, Object? right) {
	if (exists left) {
		if (exists right) {
			return left == right;
		}
	}
	return (left is Null && right is Null);
}

shared ReturnType(TargetType) orDefault<TargetType, ReturnType>(ReturnType?(TargetType) func, ReturnType defaultVal) {
	return (TargetType target) => func(target) else defaultVal;
}

shared Integer nullableHash(Object? obj) {
	if (exists obj) {
		return obj.hash;
	} else {
		return 0;
	}
}

shared class CollectingCommandsConsumer() satisfies CommandsConsumer {
	variable Command[] commands = [];
	shared actual void consumeCommand(Command command) {
		commands = commands.withTrailing(command);
	}
	shared Command[] collectedCommands => commands;
}