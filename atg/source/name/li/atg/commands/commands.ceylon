import ceylon.collection {
	TreeSet
}

import name.li.atg {
	Property,
	nullalbeEquals,
	nullableHash,
	orDefault
}

shared class Id(shared String val, shared String itemDescriptor) {
	shared actual Boolean equals(Object that) {
		if (is Id that) {
			return val==that.val &&
					itemDescriptor==that.itemDescriptor;
		}
		else {
			return false;
		}
	}
	shared actual Integer hash {
		variable value hash = 1;
		hash = 31*hash + val.hash;
		hash = 31*hash + itemDescriptor.hash;
		return hash;
	}

	shared actual String string => "Id (``val``, ``itemDescriptor``)";
}

shared abstract class Command(shared Id id, shared String snapshot) of AddItem | UpdateItem | RemoveItem {
}

shared class AddItem(Id id, String snapshot, {SetProperty*} mutationsSeq) extends Command(id, snapshot) {
	TreeSet<SetProperty> mutationsSet = TreeSet { elements = mutationsSeq; compare = byPropertyNameAndValue; };

	shared actual Boolean equals(Object that) {
		if (is AddItem that) {
			return id==that.id &&
					snapshot==that.snapshot &&
					mutations==that.mutations;
		}
		else {
			return false;
		}
	}

	shared actual Integer hash {
		variable value hash = 1;
		hash = 31*hash + id.hash;
		hash = 31*hash + snapshot.hash;
		hash = 31*hash + mutations.hash;
		return hash;
	}

	shared actual String string => "AddItem ``id``|``snapshot``, mutations: ``mutations``";

	shared Set<SetProperty> mutations => this.mutationsSet;
}

shared class SetProperty(shared Property prop, shared String? val) {
	shared actual String string => "Set property ``prop.name`` = `` val else "null" ``";

	shared actual Boolean equals(Object that) {
		if (is SetProperty that) {
			return prop==that.prop &&
					nullalbeEquals(val, that.val);
		}
		else {
			return false;
		}
	}

	shared actual Integer hash => prop.hash + nullableHash(val);
}

shared class UpdateItem(Id id, String snapshot, shared {SetProperty+} mutationsSeq) extends Command(id, snapshot) {
	TreeSet<SetProperty> mutationsSet = TreeSet { elements = mutationsSeq; compare = byPropertyNameAndValue; };

	shared actual Boolean equals(Object that) {
		if (is UpdateItem that) {
			return id==that.id &&
					snapshot==that.snapshot &&
					mutations==that.mutations;
		}
		else {
			return false;
		}
	}

	shared actual Integer hash {
		variable value hash = 1;
		hash = 31*hash + id.hash;
		hash = 31*hash + snapshot.hash;
		hash = 31*hash + mutations.hash;
		return hash;
	}

	shared actual String string => "UpdateItem ``id``|``snapshot``, mutations: ``mutations``";

	shared Set<SetProperty> mutations => this.mutationsSet;
}

shared class RemoveItem(Id id, String snapshot) extends Command(id, snapshot) {
	shared actual String string => "RemoveItem ``id``|``snapshot``";

	shared actual Boolean equals(Object that) {
		if (is RemoveItem that) {
			return id==that.id &&
					snapshot==that.snapshot;
		}
		else {
			return false;
		}
	}

	shared actual Integer hash {
		variable value hash = 1;
		hash = 31*hash + id.hash;
		hash = 31*hash + snapshot.hash;
		return hash;
	}
}

Comparison byPropertyNameAndValue(SetProperty s1, SetProperty s2) {
	return comparing(byIncreasing(compose(Property.name, SetProperty.prop)), byDecreasing(orDefault(SetProperty.val, "")))(s1, s2);
}