import ceylon.collection {
	HashMap
}

import name.li.atg {
	CommandsConsumer,
	Property,
	itemId
}

String unescape(String str) {
	return compose(deduplicateQuotes, trimQuotes)(str);
}

String trimQuotes(String str) {
	value quote = '"';
	if (exists firstChar = str.first) {
		if (exists lastChar = str.last) {
			if (firstChar==quote && lastChar==quote) {
				return str.trim('"'.equals);
			}
		}
	}
	return str;
}

String deduplicateQuotes(String str) {
	return str.replace("\"\"", "\"");
}

shared class CsvParser(String itemDescriptor) {
	
	shared void readCommands(Iterator<String> lines, CommandsConsumer output, String snapshot, IdGenerator idGenerator) {
		value header = lines.next();
		"Header with property names is missing"
		assert (is String header);
		
		value properties = mapPropertyToPosition(header);
		
		while (is String line = lines.next()) {
			value addItem = readAddItemCommandFromLine(line, properties, snapshot, idGenerator);
			output.consumeCommand(addItem);
		}
	}
	
	AddItem readAddItemCommandFromLine(
		String line,
		Map<Integer,Property> positionMappings,
		String snapshot,
		IdGenerator idGenerator) {
		variable SetProperty[] mutations = [];
		variable Id? idFromCsv = null;
		for (index->val in tokenizeLine(line).indexed) {
			"Invalid csv document"
			assert (exists property = positionMappings[index]);
			if (property == itemId) {
				if (!val.empty) {
					idFromCsv = Id(val, itemDescriptor);
				}
			} else {
				mutations = mutations.withTrailing(SetProperty(property, val));
			}
		}
		return AddItem(idFromCsv else Id(idGenerator.next(), itemDescriptor), snapshot, mutations);
	}
	
	{String*} tokenizeLine(String line) {
		return line.split(','.equals).map(unescape);
	}
	Map<Integer,Property> mapPropertyToPosition(String header) {
		return HashMap { entries = tokenizeLine(header).map(Property).indexed; };
	}
}
