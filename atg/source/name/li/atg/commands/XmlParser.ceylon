import ceylon.collection {
	LinkedList
}

import java.io {
	Reader,
	FileInputStream,
	InputStreamReader
}

import javax.xml.namespace {
	QName
}
import javax.xml.stream {
	XMLInputFactory,
	XMLEventReader
}
import javax.xml.stream.events {
	StartElement,
	EndElement,
	XMLEvent,
	Characters
}

import name.li.atg {
	Property,
	CommandsConsumer
}

Iterator<XMLEvent> makeIterator(XMLEventReader events) {
	object iterator satisfies Iterator<XMLEvent> {
		shared actual XMLEvent|Finished next() => if (events.hasNext()) then events.nextEvent() else finished;
	}
	return iterator;
}

shared void parseXml(String file, CommandsConsumer consumer) {
	value fileStream = FileInputStream(file);
	value nonBomStream = UnicodeBOMInputStream(fileStream);
	// XXX: some files may have BOM and xml parser fails as a result since bom mark is located before prolog
	nonBomStream.skipBOM();
	value reader = InputStreamReader(nonBomStream);

	value commands = XmlParser(file, reader, consumer).generateCommands();
	commands.each(consumer.consumeCommand);
}

shared class XmlParser(String sourceName, Reader reader, CommandsConsumer consumer) {

	shared Command[] generateCommands() {
		value factory = XMLInputFactory.newFactory();
		value events = makeIterator(factory.createXMLEventReader(reader));
		return Parser(sourceName, events, consumer,  PrefixBasedSequentialIdGenerator(sourceName)).parse();
	}
}

class Parser(String sourceName, Iterator<XMLEvent> events, CommandsConsumer consumer, IdGenerator idGenerator) {
	shared Command[] parse() {
		while (is XMLEvent event = events.next()) {
			if (is StartElement event) {
				if (event.name.localPart == "add-item") {
					value addItem = parseAddItem(events, event, sourceName, idGenerator);
					consumer.consumeCommand(addItem);
				}
				if (event.name.localPart == "update-item") {
					value updateItem = parseUpdateItem(events, event, sourceName, idGenerator);
					consumer.consumeCommand(updateItem);
				}
				if (event.name.localPart == "remove-item") {
					value removeItem = parseRemoveItem(events, event, sourceName);
					consumer.consumeCommand(removeItem);
				}
			}
		}

		return [];
	}
}

CommandType(Iterator<XMLEvent>, StartElement, String, IdGenerator)
createParserForCommandWithMutations<CommandType>
		(String tagName, CommandType(Id, String, SetProperty[]) eventConstructor) {
	return (Iterator<XMLEvent> events, StartElement element, String fileName, IdGenerator idGenerator) {
		value mutations = LinkedList<SetProperty>();
		value id = element.getAttributeByName(QName("id"))?.\ivalue else idGenerator.next();
		value itemDescriptor = element.getAttributeByName(QName("item-descriptor")).\ivalue;
		while (is XMLEvent event = events.next()) {
			if (is StartElement event) {
				assert (event.name.localPart == "set-property");
				value setProperty = parseSetProperty(events, event);
				mutations.add(setProperty);
			}
			if (is EndElement event) {
				if (event.name.localPart == tagName) {
					return eventConstructor(Id(id, itemDescriptor), fileName, mutations.sequence());
				}
			}
		}
		throw Exception("Parsing failed, something is wrong =(");
	};
}

AddItem(Iterator<XMLEvent>, StartElement, String, IdGenerator) parseAddItem = createParserForCommandWithMutations("add-item", `AddItem`);

UpdateItem(Id, String, SetProperty[]) updateItemConstructor = (Id id, String snapshot, SetProperty[] mutations) {
	if (nonempty mutations) {
		return UpdateItem(id, snapshot, mutations);
	} else {
		throw Exception("UpdateItem should have at least one mutation");
	}
};

UpdateItem(Iterator<XMLEvent>, StartElement, String, IdGenerator) parseUpdateItem = createParserForCommandWithMutations("update-item", updateItemConstructor);

SetProperty parseSetProperty(Iterator<XMLEvent> events, StartElement element) {
	assert (element.name.localPart == "set-property");


	String? attributeValue = element.getAttributeByName(QName("value"))?.\ivalue;
	value prop = element.getAttributeByName(QName("name")).\ivalue;
	if (exists attributeValue) {
		//value is in attribute
		assert (is EndElement event = events.next()); // make sure this is self-closing tag
		return SetProperty(Property(prop), attributeValue);
	} else {
		//value is text inside element
		value textValue = parseTextPropertyValue(events, element);
		return SetProperty(Property(prop), textValue);
	}
}


String parseTextPropertyValue(Iterator<XMLEvent> events, StartElement setPropertyElement) {
	variable String? text = null;
	while (is XMLEvent event = events.next()) {
		if (is Characters event) {
			value trimSpaces = (String str) => str.trim(['\t',' ', '\n'].contains);
			value eventText = if (event.cData) then event.data else trimSpaces(event.data);
			text = (text else "") + eventText;
		}
		if (is EndElement event) {
			"Something is wrong: closing tag does not match opening tag"
			assert(event.name == setPropertyElement.name);
			break;
		}
	}
	"Failed to extract property value from element ``setPropertyElement.name``"
	assert(exists checkedText = text);
	return checkedText;
}

RemoveItem parseRemoveItem(Iterator<XMLEvent> events, StartElement removeItemTag, String sourceName) {
	value nextEvent = events.next();

	"Expected end of remove-item tag"
	assert(is EndElement nextEvent);
	"Expected end of remove-item tag"
	assert(nextEvent.name.localPart == removeItemTag.name.localPart);

	value id = removeItemTag.getAttributeByName(QName("id"))?.\ivalue;
	value itemDescriptor = removeItemTag.getAttributeByName(QName("item-descriptor"))?.\ivalue;

	"id attribute is not present at the element ``removeItemTag``"
	assert(exists id);
	"item-descriptor attribute is not present at the element: ``removeItemTag``"
	assert(exists itemDescriptor);

	return RemoveItem(Id(id, itemDescriptor), sourceName);
}
