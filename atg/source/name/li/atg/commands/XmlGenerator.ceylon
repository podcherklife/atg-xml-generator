import java.io {
	Writer
}

import javax.xml.stream {
	XMLStreamWriter,
	XMLOutputFactory
}
shared class XmlGenerator() {

	shared void generate(Writer output, Command[] commands) {
		checkAllCommandsBelongToTheSameSnapshot(commands);

		value factory = XMLOutputFactory.newFactory();
		value xmlStream = factory.createXMLStreamWriter(output);

		xmlStream.writeStartDocument();
		xmlStream.writeStartElement("gsa-template");
		commands.each(curry(writeCommand)(xmlStream));
		xmlStream.writeEndElement();
		xmlStream.writeEndDocument();
	}
	void checkAllCommandsBelongToTheSameSnapshot(Command[] commands) {
		value snapshots = commands.map((command) {
			switch(command)
			case(is AddItem) {
				return command.snapshot;
			}
			case(is UpdateItem) {
				return command.snapshot;
			}
			case(is RemoveItem) {
				return command.snapshot;
			}
		}).distinct;

		"Commands belong to the different snapshots"
		assert (snapshots.size == 1);
	}

	void writeCommand(XMLStreamWriter output, Command command) {
		switch(command)
		case(is AddItem) {
			writeAddItem(output, command);
		}
		case(is UpdateItem) {
			writeUpdateItem(output, command);
	 	}
	 	case(is RemoveItem) {
	 		writeRemoveItem(output, command);
	 	}
	}

	void writeRemoveItem(XMLStreamWriter output, RemoveItem command) {
		output.writeStartElement("remove-item");
		output.writeAttribute("id", command.id.val);
		output.writeAttribute("item-descriptor", command.id.itemDescriptor);
		output.writeEndElement();
	}

	void writeUpdateItem(XMLStreamWriter output, UpdateItem command) {
		output.writeStartElement("update-item");
		output.writeAttribute("id", command.id.val);
		output.writeAttribute("item-descriptor", command.id.itemDescriptor);
		command.mutations.each(curry(writeSetProperty)(output));
		output.writeEndElement();
	}

	void writeAddItem(XMLStreamWriter output, AddItem command) {
		output.writeStartElement("add-item");
		output.writeAttribute("id", command.id.val);
		output.writeAttribute("item-descriptor", command.id.itemDescriptor);
		command.mutations.each(curry(writeSetProperty)(output));
		output.writeEndElement();
	}

	void writeSetProperty(XMLStreamWriter output, SetProperty command) {
		output.writeStartElement("set-property");
		output.writeAttribute("name", command.prop.name);
		output.writeCData(command.val);
		output.writeEndElement();
	}
}

