class IdGenerators() {}

shared interface IdGenerator {
	shared formal String next();
}

shared class PrefixBasedSequentialIdGenerator(String prefix) satisfies IdGenerator {
	variable Integer index = 0;
	shared actual String next() => "``prefix```` index++ ``";
}
