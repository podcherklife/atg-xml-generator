#! /bin/bash
pushd ..
./ceylonb compile
./ceylonb run name.li.atg -baseline=./example/data/ -input=./example/v3.csv -applyChanges=./example/apply.xml -rollbackChanges=./example/rollback.xml
popd
