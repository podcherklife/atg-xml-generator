#! /bin/bash
pushd ..
./ceylonb compile
./ceylonb run name.li.atg -baseline=./example-with-track-by/data/ -input=./example-with-track-by/v3.csv -applyChanges=./example-with-track-by/apply.xml -rollbackChanges=./example-with-track-by/rollback.xml -generatedIdPrefix=REPLACE_ME_WITH_SANE_VALUE -trackBy=key
popd
